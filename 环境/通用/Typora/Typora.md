# Typora

一款 Markdown 编辑器和阅读器

下载安装 [https://typoraio.cn](https://typoraio.cn)

# 破解

> Typora 1.2.4

将 [app.asar](app.asar) 文件 替换 `Typora\resources\app.asar`文件 即可

```
# 个人安装路径
D:\zhengqingya\soft\soft-dev\Typora\resources\app.asar

# 默认路径
C:\Program Files\Typora\resources\app.asar
```

### 主题

下载`softgreen`主题： https://theme.typoraio.cn/theme/softgreen

1. 放至主题文件夹下 `C:\Users\zhengqingya\AppData\Roaming\Typora\themes`
2. 重启`Typora`